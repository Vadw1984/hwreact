import React, { Component, useMemo, Fragment } from 'react';
// import { Redact } from './Redact/Redact';
import { BookListItem } from './BookListItem/BookListItem';

export class BookList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const BookListItemRender = this.props.NewBooks.map((item, index) => {
      return <BookListItem index={index} {...this.props} />;
    });

    return (
      <div>
        <table className="table table-striped mt-2">
          <thead>
            <tr>
              <th>Title</th>
              <th>Author</th>
              <th>ISBN#</th>
              <th>Title</th>
            </tr>
          </thead>
          {BookListItemRender}  
        </table>
      </div>
    );
  }
}
