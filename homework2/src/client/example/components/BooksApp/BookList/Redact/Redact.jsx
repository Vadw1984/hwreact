import React from 'react';

export const Redact = () => {
  return (
    <form action="" id="update-book-form">
      <tr>
        <td>
          <div className="form-group">
            <input
              type="text"
              name="book-name"
              className="form-control"
              value=""
            />
          </div>
        </td>
        <td>
          <div className="form-group">
            <input
              type="text"
              name="book-author"
              className="form-control"
              value=""
            />
          </div>
        </td>
        <td>
          <div className="form-group">
            <input
              type="text"
              name="book-isbn"
              className="form-control"
              value=""
            />
          </div>
        </td>
        <td>
          <input type="submit" value="Update" className="btn btn-primary" />
        </td>
        <td></td>
      </tr>
    </form>
  );
};
