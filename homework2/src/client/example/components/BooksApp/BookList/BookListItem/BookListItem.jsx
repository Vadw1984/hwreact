import React, { Component } from 'react';

export class BookListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      IsUpdate: false,
      BookName: '',
      BookAuthor: '',
      BookIsbn: '',
    };
  }

  RemoveBook = (event) => {
    return event.preventDefault(), this.props.RemoveBooks(this.props);
  };

  AddBook = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  PrepareToEdit = (event) => {
    return (
      event.preventDefault(), this.setState({ IsUpdate: !this.state.IsUpdate })
    );
  };

  UpdateBook = (event) => {
    event.preventDefault();
    const { BookName, BookAuthor, BookIsbn } = this.state;

    const newBookElem = {
      BookName: BookName,
      BookAuthor: BookAuthor,
      BookIsbn: BookIsbn,
      IndexEl: this.props.index,
    };

    this.setState({ IsUpdate: !this.state.IsUpdate });
    this.props.UpdateBooks(newBookElem);
  };

  render() {
    const { BookName, BookAuthor, BookIsbn } = this.state;

    if (!this.state.IsUpdate) {
      return (
        <tbody id="book-list">
          <form action="" id="update-book-form"></form>
          <tr data-id="12">
            <td>
              <div class="form-group">
                <input
                  type="text"
                  name="book-name"
                  class="form-control"
                  value={this.props.NewBooks[this.props.index].BookName}
                />
              </div>
            </td>
            <td>
              <div class="form-group">
                <input
                  type="text"
                  name="book-author"
                  class="form-control"
                  value={this.props.NewBooks[this.props.index].BookAuthor}
                />
              </div>
            </td>
            <td>
              <div class="form-group">
                <input
                  type="text"
                  name="book-isbn"
                  class="form-control"
                  value={this.props.NewBooks[this.props.index].BookIsbn}
                />
              </div>
            </td>
            <td>
              <a
                onClick={this.PrepareToEdit}
                href="#"
                className="btn btn-info btn-sm">
                <i className="fas fa-edit"></i>
              </a>
            </td>
            <td>
              <a
                onClick={this.RemoveBook}
                href="#"
                className="btn btn-danger btn-sm btn-delete">
                X
              </a>
            </td>
          </tr>
        </tbody>
      );
    } else {
      return (
        <tbody id="book-list">
          <form action="" id="update-book-form"></form>
          <tr data-id="12">
            <td>
              <div class="form-group">
                <input
                  type="text"
                  name="BookName"
                  class="form-control"
                  onChange={this.AddBook}
                  value={BookName}
                />
              </div>
            </td>
            <td>
              <div class="form-group">
                <input
                  type="text"
                  name="BookAuthor"
                  class="form-control"
                  onChange={this.AddBook}
                  value={BookAuthor}
                />
              </div>
            </td>
            <td>
              <div class="form-group">
                <input
                  type="text"
                  name="BookIsbn"
                  class="form-control"
                  onChange={this.AddBook}
                  value={BookIsbn}
                />
              </div>
            </td>

            <td>
              <input
                onClick={this.UpdateBook}
                type="submit"
                value="Update"
                className="btn btn-primary"
              />
            </td>
            <td></td>
          </tr>
        </tbody>
      );
    }
  }
}
