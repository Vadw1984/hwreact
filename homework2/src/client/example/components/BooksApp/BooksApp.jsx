import React, { Component } from 'react';
import { AddBookForm } from './AddBookForm/AddBookForm';
import { BookList } from './BookList/BookList';
import { BookCount } from './BookCount/BookCount';

export class BooksApp extends Component {
  state = {
    NewBooks: [],
  };

  HandleClickSubmitRow = (event) => {
    return event.preventDefault();
  };

  SetBooks = (newBookElem) => {
    newBookElem.IsUpdate = false;
    this.setState({
      NewBooks: [...this.state.NewBooks, newBookElem],
    });
  };

  RemoveBooks = (RemoveBookElem) => {
    const RemoveBook = this.state.NewBooks;
    RemoveBook.splice(RemoveBookElem.index, 1);
    this.setState({ NewBooks: RemoveBook });
  };

  UpdateBooks = (UpdateBookElem) => {
    const UpdateArr = this.state.NewBooks;

    UpdateArr.splice(UpdateBookElem.IndexEl, 1, {
      ...UpdateArr[UpdateBookElem.IndexEl],
      ...UpdateBookElem,
    });
    this.setState({
      NewBooks: this.state.NewBooks,
      UpdateArr,
    });
  };

  render() {
    return (
      <div>
        <AddBookForm
          {...this.state}
          SetBooks={this.SetBooks}
          HandleClickSubmitRow={this.HandleClickSubmitRow}
        />
        <BookCount count={this.state.NewBooks.length} />
        <BookList
          {...this.state}
          SetBooks={this.SetBooks}
          RemoveBooks={this.RemoveBooks}
          UpdateBooks={this.UpdateBooks}
        />
      </div>
    );
  }
}
