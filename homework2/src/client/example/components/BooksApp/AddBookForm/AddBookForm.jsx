import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class AddBookForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      IsBookAdd: false,
      BookName: '',
      BookAuthor: '',
      BookIsbn: ''
    };
  }

  AddBook = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  AddBooksInState = () => {
    const { BookName, BookAuthor, BookIsbn } = this.state;

    const newBookElem = {
      BookName: BookName,
      BookAuthor: BookAuthor,
      BookIsbn: BookIsbn,
    };

    return this.props.SetBooks(newBookElem);
  };
  render() {
    const { BookName, BookAuthor, BookIsbn } = this.state;

    return (
      <div>
        <h1 className="display-4 text-center">
          <i className="fas fa-book-open text-primary"></i>{' '}
          <span className="text-secondary">Book</span> List
        </h1>
        <p className="text-center">
          Add your book information to store it in database.
        </p>
        <div className="row">
          <div className="col-lg-4">
            <form
              action="#"
              onSubmit={this.props.HandleClickSubmitRow}
              id="add-book-form">
              <div className="form-group">
                <label>Title</label>
                <input
                  type="text"
                  name="BookName"
                  className="form-control"
                  onChange={this.AddBook}
                  value={BookName}
                />
              </div>
              <div className="form-group">
                <label>Author</label>
                <input
                  type="text"
                  name="BookAuthor"
                  className="form-control"
                  onChange={this.AddBook}
                  value={BookAuthor}
                />
              </div>
              <div className="form-group">
                <label>ISBN#</label>
                <input
                  type="text"
                  name="BookIsbn"
                  className="form-control"
                  onChange={this.AddBook}
                  value={BookIsbn}
                />
              </div>
              <input
                type="submit"
                onClick={this.AddBooksInState}
                value="Add Book"
                className="btn btn-primary"
              />
            </form>
          </div>
        </div>
      </div>
    );
  }
}

// AddBookForm.propTypes = {
//   SetBooks: PropTypes.func
// };