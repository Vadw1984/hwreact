import React, { Component } from 'react';

export class BookCount extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <h3 id="book-count" className="book-count mt-5">
          Всего книг: {this.props.count}
        </h3>
      </>
    );
  }
}
