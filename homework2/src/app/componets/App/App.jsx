import React from 'react';
import '../../../shared/styles/scss/style.scss';

import { BooksApp } from '../../../client/example/components/BooksApp';

export const App = () => {
  return (
    <div className="container mt-4">
      <BooksApp />
    </div>
  );
};
