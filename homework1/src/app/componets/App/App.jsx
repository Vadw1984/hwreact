import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/scss/style.scss';

// if you using css deleting scss import, and uncomment this import
// import '../../../shared/styles/scss/style.css';

import { Example } from '../../../client/example/components/Example';

export const App = () => {
  return (
    <div>
      <Example />
    </div>
  );
};
