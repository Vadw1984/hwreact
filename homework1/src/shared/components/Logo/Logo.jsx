import React from 'react';
import './Logo.css';

export const Logo = (props) => {
  const { type } = props;
  let typeLogo = '';
  type === 'LogoFooter' ? (typeLogo = 'LogoFooter') : (typeLogo = 'Logo');
  console.log(typeLogo);

  return (
    <>
      <p className={typeLogo}>movie</p>
    </>
  );
};
