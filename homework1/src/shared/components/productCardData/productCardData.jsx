export const productCardData = [
  {
    urlPhoto:
      'https://res.cloudinary.com/dk88eyahu/image/upload/v1596473214/rhw1/photoFilms/AssAssin_s_Creed_f5fm0d.png',
    name: 'Fantastic Beasts',
    rate: '4.7',
    genre: ['Adventure', 'Family', 'Fantasy'],
  },
  {
    urlPhoto:
      'https://res.cloudinary.com/dk88eyahu/image/upload/v1596473327/rhw1/photoFilms/Captain_America..._1X_nmklfa.png',
    name: 'AssAssin’s Creed',
    rate: '4.2',
    genre: ['Action', 'Adventure', 'Fantasy'],
  },
  {
    urlPhoto:
      'https://res.cloudinary.com/dk88eyahu/image/upload/v1596473332/rhw1/photoFilms/Fantastic_Beasts..._pfazam.png',
    name: 'Captain America',
    rate: '4.9',
    genre: ['Action', 'Adventure', 'Sci - Fi'],
  },
  {
    urlPhoto:
      'https://res.cloudinary.com/dk88eyahu/image/upload/v1596485995/rhw1/photoFilms/The_Legend_of_Tarzan_vhjmt9.png',
    name: 'The Legend of Tarzan',
    rate: '4.3',
    genre: ['Action', 'Adventure', 'Drama'],
  },
  {
    urlPhoto:
      'https://res.cloudinary.com/dk88eyahu/image/upload/v1596485994/rhw1/photoFilms/doctorstrange1_1X_qyuyvr.png',
    name: 'Doctor Strange',
    rate: '4.8',
    genre: ['Action', 'Adventure', 'Fantasy'],
  },
  {
    urlPhoto:
      'https://res.cloudinary.com/dk88eyahu/image/upload/v1596473214/rhw1/photoFilms/AssAssin_s_Creed_f5fm0d.png',
    name: 'X-MAN',
    rate: '4.7',
    genre: ['Action', 'Adventure', 'Fantasy'],
  },
  {
    urlPhoto:
      'https://res.cloudinary.com/dk88eyahu/image/upload/v1596485994/rhw1/photoFilms/Finding_Dory_zdyokb.png',
    name: 'Finding Dory',
    rate: '4.9',
    genre: ['Action', 'Adventure', 'Comedy'],
  },
  {
    urlPhoto:
      'https://res.cloudinary.com/dk88eyahu/image/upload/v1596485995/rhw1/photoFilms/The_BFG_mtdo5u.png',
    name: 'The BFG',
    rate: '4.6',
    genre: ['Action', 'Adventure', 'Fantasy'],
  },
  {
    urlPhoto:
      'https://res.cloudinary.com/dk88eyahu/image/upload/v1596485994/rhw1/photoFilms/Now_you_see_me_2_bymcno.png',
    name: 'Now you see me 2',
    rate: '4.4',
    genre: ['Action', 'Adventure', 'Comedy'],
  },
  {
    urlPhoto:
      'https://res.cloudinary.com/dk88eyahu/image/upload/v1596473332/rhw1/photoFilms/Fantastic_Beasts..._pfazam.png',
    name: 'Independence Day',
    rate: '3.9',
    genre: ['Action', 'Adventure', 'Sci - Fi'],
  },
  {
    urlPhoto:
      'https://res.cloudinary.com/dk88eyahu/image/upload/v1596485995/rhw1/photoFilms/The_BFG_mtdo5u.png',
    name: 'The BFG',
    rate: '4.6',
    genre: ['Action', 'Adventure', 'Fantasy'],
  },
  {
    urlPhoto:
      'https://res.cloudinary.com/dk88eyahu/image/upload/v1596485995/rhw1/photoFilms/The_Legend_of_Tarzan_vhjmt9.png',
    name: 'The Legend of Tarzan',
    rate: '4.3',
    genre: ['Action', 'Adventure', 'Drama'],
  },
];
