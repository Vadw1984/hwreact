import React from 'react';
import './Rating.css';

const Rating = (props) => {
  const { typeRating } = props;
  let classRating = '';

  typeRating === 'white'
    ? (classRating = 'RatingScore')
    : (classRating = 'RatingScoreBlu');

  return (
    <div className="Rating">
      <p className={classRating}>{props.text}</p>
    </div>
  );
};

export default Rating;
