import React from 'react';
import './Loading.css';

const Loading = () => {
  return (
    <div className="LoadingContainer">
      <div className="Loading"></div>
      <p className="LoadingText">Loading</p>
    </div>
  );
};
export default Loading;
