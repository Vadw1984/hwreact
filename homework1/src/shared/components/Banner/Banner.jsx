import React from 'react';
import './Banner.css';
import '../../styles/css/style.css';
import { ButtonHeader } from '../ButtonHeader/ButtonHeader';

const Banner = (props) => {
  const { text } = props;

  return (
    <div className="Banner">
      <div className="transparentBanner flexCol">
        <p className="BannerText">{text[0].text}</p>
        <ButtonHeader bg='BigBlue' text='Subscribe'/>
      </div>
    </div>
  );
};

export default Banner;
