import React from 'react';
import './Description.css'

const Description = (props) => {

const classDescription = props.color==="blue"?"DescriptionBlue":'gray'?'DescriptionGray':'Description'

return <p className={classDescription}>{props.text}</p>;
};
export default Description;
