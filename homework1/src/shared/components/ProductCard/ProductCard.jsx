import React from 'react';
import './ProductCard.css';
import Rating from '../Rating/Rating';
import Description from '../Description/Description';
import '../../styles/css/style.css';

export const ProductCard = (props) => {
  const { urlPhoto, name, rate, genre } = props;

  const filmGenre = genre.map((item) => {
    return <Description color="blue" text={item} />;
  });

  return (
    <div className="ProductCard">
      <div
        className="imageProductCart"
        style={{ backgroundImage: `url(${urlPhoto})` }}
        alt="#"></div>
      <div className="ProductCardInfo" >
        <div className="flexCol" style={{ alignItems: `start` }}> 
          <h3 className="ProductCardName">{name}</h3>
          <div className="flexRow ProductCardFilmGenre">{filmGenre}</div>
        </div>
        <Rating text={rate} />
      </div>
    </div>
  );
};
