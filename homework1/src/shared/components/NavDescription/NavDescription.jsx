import React from 'react';
import './NavDescription.css'

const NavDescription = (props) => {
    const NavSectionMenuItems = [
        'Trending',
        'Top Rated',
        'New Arrivals',
        'Trailers',
        'Coming Soon',
        'Genre',
      ];
      const NavItems = NavSectionMenuItems.map((item) => {
        return <p className="NavDescription">{item}</p>
      });

return <p className="NavDescription_" style={{display:'flex'}}>{NavItems}</p>;
};
export default NavDescription;
