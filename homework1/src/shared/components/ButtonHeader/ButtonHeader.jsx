import React from 'react';
import './ButtonHeader.css';

export const ButtonHeader = (props) => {
  let propsButton = props.bg;
  let className = 'buttonClassBlue';

  if (propsButton === 'blue') {
    className = 'buttonClassBlue';
  } else if (propsButton === 'transBorder'){
    className = 'transBorder'
  } else if (propsButton === 'BigBlue'){
    className = 'buttonClassBig'
  }else className = 'buttonClassTransparent';

return <button className={className}>{props.text}</button>;
};
