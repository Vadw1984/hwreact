import React from 'react';
import { Header } from '../Header/Header';
import { Section } from '../Section/Section';
import { Footer } from '../Footer';

export const Example = () => {
  return (
    <div className="ContainerSection">
      <Header />
      <Section />
      <Footer />
    </div>
  );
};
