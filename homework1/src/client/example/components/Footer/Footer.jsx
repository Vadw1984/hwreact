import React from 'react';

import './Footer.css';
import { Logo } from '../../../../shared/components/Logo/Logo';
import Description from '../../../../shared/components/Description/Description';
import '../../../../shared/styles/css/style.css';

export const Footer = () => {
  const headerMenuArr = ['About', 'Terms of Service', 'Contact'];

  const headerMenuItems = headerMenuArr.map((item) => {
    return <Description text={item} color="gray" />;
  });

  return (
    <div className="Footer flexCol">
      <div className="FooterTop flexRow">
        <div className="FooterMenu flexRow">{headerMenuItems}</div>
        <Logo type="LogoFooter" />
        <div className="social-network"></div>
      </div>
      <div className="FooterBottom ">
        <p>Copyright © 2017 MOVIERISE. All Rights Reserved.</p>
      </div>
    </div>
  );
};
