import React from 'react';
import './PhotoSection.css';
import { ProductCard } from '../../../../../shared/components/ProductCard/ProductCard';
import { productCardData } from '../../../../../shared/components/productCardData/productCardData';
import { NavSection } from './NavSection';
import Banner from '../../../../../shared/components/Banner/Banner';
import { BannerInfo } from '../../../../../shared/components/Banner/BannerInfo/BannerInfo';
import Loading from '../../../../../shared/components/Loading/Loading';

export const PhotoSection = () => {
  
  const TopSection = productCardData.map((item, index) => {
    if (index <= 11) {
      return (
        <ProductCard
          urlPhoto={item.urlPhoto}
          name={item.name}
          rate={item.rate}
          genre={item.genre}
        />
      );
    }
  });

  const LowSection = productCardData.map((item, index) => {
    if (index <= 3) {
      return (
        <ProductCard
          urlPhoto={item.urlPhoto}
          name={item.name}
          rate={item.rate}
          genre={item.genre}
        />
      );
    }
  });

  return (
    <div>
      <NavSection />
      <div className="PhotoSection">{TopSection}</div>
      <Banner text={BannerInfo} />
      <div className="PhotoSection">{LowSection}</div>
      <Loading/>
    </div>
  );
};
