import React from 'react';
import './MenuVisibility.css';

export const MenuVisibility = () => {
  return (
    <div className="MenuVisibility">
      <div className="VarVisibility">
        <div className="Squire">
          <div className="fourSquire"></div>
          <div className="fourSquire"></div>
          <div className="fourSquire"></div>
          <div className="fourSquire"></div>
        </div>
        <div className="Squire">
          <div className="twoSquire"></div>
          <div className="twoSquire"></div>
        </div>
      </div>
    </div>
  );
};
