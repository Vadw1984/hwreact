import React from 'react';
import './NavSection.css';
import NavDescription from '../../../../../../shared/components/NavDescription/NavDescription';
import { MenuVisibility } from './MenuVisibility/MenuVisibility';

export const NavSection = () => {
  return (
    <div className="NavSection">
      <div className="NavSectionMenu">
        <NavDescription />
      </div>
      <MenuVisibility />
    </div>
  );
};
