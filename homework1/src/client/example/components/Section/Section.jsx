import React from 'react';
import './Section.css';

import { NavSection } from './PhotoSection/NavSection';
import { PhotoSection } from './PhotoSection/PhotoSection';

export const Section = () => {
  return (
    <div className="PhotoSectionContainer">
      <div className="Section">
        <PhotoSection />
      </div>
    </div>
  );
};
