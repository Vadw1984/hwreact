import React from 'react';
import './nav-header.css';
import { Auth } from './Auth/Auth';
import './nav-header.css'
import { Logo } from '../../../../../shared/components/Logo/Logo';

export const NavHeader = () => {
  return (
    <div className="nav-header">
      <Logo />
      <Auth />
    </div>
  );
};
