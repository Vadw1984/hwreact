import React from 'react';
import './auth.css';
import { ButtonHeader } from '../../../../../../shared/components/ButtonHeader/ButtonHeader';

export const Auth = () => {
  return (
    <div className="auth">
      <img className='finder'
        src={require('../../../../../../assets/images/Search Icon@1X.png')}
        alt="finder"
        ></img>
      <ButtonHeader text="Sign in" />
      <ButtonHeader bg="blue" text="Sign Up" />
    </div>
  );
};
