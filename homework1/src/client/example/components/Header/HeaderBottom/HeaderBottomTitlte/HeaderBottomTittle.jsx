import React from 'react';
import './HeaderBottomTittle.css';
import '../../../../../../shared/styles/css/style.css';
import Description from '../../../../../../shared/components/Description/Description';

const HeaderBottomTittle = () => {
  return (
    <div className="HeaderBottomTittleContainer">
      <h1 className="HeaderBottomTittle">The Jungle Book</h1>
      <div className="flexRow">
        <Description text="Adventure" />
        <Description text="Drama" />
        <Description text="Family" />
        <Description text="Fantasy" />
        <Description text="   |   " />
        <Description text="1h 46m" />
      </div>
    </div>
  );
};

export default HeaderBottomTittle;
