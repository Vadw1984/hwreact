import React from 'react';
import './HeaderBottom.css';
import HeaderBottomTittle from './HeaderBottomTitlte/HeaderBottomTittle';
import {HeaderBottomNav} from './HeaderBottomNav';

export const HeaderBottom = () => {
  return (
    <div className="HeaderBottom">
      <HeaderBottomTittle/>
      <HeaderBottomNav/>
    </div>
  );
};
