import React from 'react';
import './HeaderBottomNav.css';
import '../../../../../../shared/styles/css/style.css';
import StarFive from '../../../../../../shared/components/StarFive/StarFive';
import { ButtonHeader } from '../../../../../../shared/components/ButtonHeader/ButtonHeader';
import Circle from '../../../../../../shared/components/Circle/Circle';
import Rating from '../../../../../../shared/components/Rating/Rating';

export const HeaderBottomNav = () => {
  return (
    <div className="HeaderBottomNav">
      <div style={{display:'flex'}}>
        <StarFive />
        <StarFive />
        <StarFive />
        <StarFive />
        <StarFive />
        <Rating typeRating='white' text="4.8"/>
      </div>
      <div className='flexRow'style={{width:'670px'}}>
        <ButtonHeader bg="blue" text="Watch Now" />
        <ButtonHeader  bg='transBorder' text="View Info"/>
        <ButtonHeader  text="+ Favorites"/>
        <div className='flexCol'>
          <Circle/>
          <Circle/>
          <Circle/>
        </div>
      </div>
    </div>
  );
};

// export default HeaderBottomNav;
