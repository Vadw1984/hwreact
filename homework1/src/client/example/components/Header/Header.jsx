import React from 'react';
import { NavHeader } from './NavHeader';
import './header.css';
import '../../../../shared/styles/css/style.css';
import { HeaderBottom } from './HeaderBottom/HeaderBottom';

export const Header = () => {
  return (
    <div className="header ">
      <div className="transparent">
        <div className='flexCol'>
          <NavHeader/>
          <HeaderBottom />
        </div>
      </div>
    </div>
  );
};
