import React, { useState, useEffect, memo, useCallback } from 'react';

const WorkerList = props => {
  const { Page } = props;

  const [State, SetState] = useState({
    IsLoaded: false,
    IsError: null,
    RenderWorker: [],
  });

  useEffect(() => {
    fetch(`http://danit.com.ua/workers?page=${Page}`)
      .then(response => response.json())
      .then(
        result => {
          let employee = result.map(({ name, job, phone, email }) => [
            <p>
              name: {name},job: {job},phone: {phone},email: {email}
            </p>,
          ]);
          SetState({
            IsLoaded: true,
            RenderWorker: employee,
          });
        },
        error => {
          SetState({
            IsLoaded: true,
            IsError: error,
          });
        },
      );
  }, [props.Page]);

  console.log(State.RenderWorker);

  const func = useCallback(() => {
    if (State.sError) {
      return <div>Error: {State.IsError.message}</div>;
    } else if (!State.IsLoaded) {
      return <div>Loading...</div>;
    } else {
      return State.RenderWorker;
    }
  }, [State,]);
  return <div className="Worker">{func()}</div>;
};

export const Worker = memo(WorkerList);
