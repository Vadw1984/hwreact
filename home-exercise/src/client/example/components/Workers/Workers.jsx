import React, { useState, memo, useCallback } from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import { Worker } from './Worker';
import { NavigBar } from './NavBar/pages';
import { Context } from './Context';

const WorkersList = () => {
  const [Page, SetPage] = useState(1);

  const LinkNumber = useCallback(e => {
    return SetPage(e.target.text);
  },[Page,SetPage]);

  return (
    <Context.Provider value={{ LinkNumber }}>
      <Router>
        <Route exact path={'/'}>
          Для просмотрта нажмите страницу внизу
        </Route>

        <Route exact path={'/' + Page}>
          <Worker Page={Page} />
        </Route>

        <div className="Workers">
          <NavigBar LinkNumber={LinkNumber}/>
        </div>
      </Router>
    </Context.Provider>
  );
};

export const Workers= memo(WorkersList)