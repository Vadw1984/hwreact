import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { Context } from '../../Context';
import './NavigBar.css';

export const NavigBar = (props) => {
  const {LinkNumber} = props
  // const { LinkNumber } = useContext(Context);

  return (
    <ul className="NavigBar">
      <li>
        <Link onClick={LinkNumber} to="/1">
          1
        </Link>
      </li>
      <li>
        <Link onClick={LinkNumber} to="/2">
          2
        </Link>
      </li>
      <li>
        <Link onClick={LinkNumber} to="/3">
          3
        </Link>
      </li>
    </ul>
  );
};
