import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/scss/style.scss';

import {Workers } from "../../../client/example/components/Workers";

export const App = () => {
    return (
        <div className="container">
            <h1>EMPLOYEE!</h1>
            <Workers/>
        </div>
    );
};
