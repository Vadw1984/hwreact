import { initialState } from '../initialState/initialState';

const increment = (el, sign) => {
  let newEl;

  sign === '+'
    ? (newEl = el.modalTotalCount + 1)
    : (newEl = el.modalTotalCount - 1);

  if (newEl < 0) {
    el.modalTotalCount = 0;
    return el;
  } else {
    el.modalTotalCount = newEl;
    return el;
  }
};

export function modalReducer(state = initialState, { type, elem }) {
  switch (type) {
    case 'INCREMENT_CARD':
      // localStorage.setItem("state", "state");
      const incrementProduct = state.products.map(el =>
        el.id === elem ? increment(el, '+') : el,
      );

      return {
        ...state,
        products: incrementProduct,
      };

    case 'DECREMENT_CARD':
      const decrementProduct = state.products.map(el =>
        el.id === elem ? increment(el, '-') : el,
      );

      return {
        ...state,
        products: decrementProduct,
      };

    case 'CLEAR_CURRENT_CARD':
      const clearCurrentProduct = state.products.map(el => {
        if (el.id === elem) {
          el.modalTotalCount = 0;
          return el;
        } else return el;
      });

      return {
        ...state,
        products: clearCurrentProduct,
      };

    case 'DELETE_ALL_PRODUCT':
      const clearAllProduct = state.products.map(el => {
        el.modalTotalCount = 0;
        return el;
      });
      return {
        ...state,
        products: clearAllProduct,
      };

    case 'OPEN_MODAL':
      return {
        ...state,
        isModalOpen: !state.isModalOpen,
      };

    case 'SET_ALL_SUM':
      return {
        ...state,
        modalTotalPrice: elem,
      };

    default:
      return state;
  }
}
