import React from 'react';
import { ProductCard } from '../../../../shared/components/ProductCard/ProductCard';
import { initialState } from '../initialState/initialState';

export const Main = props => {
  const productList = initialState.products.map(
    ({ cardTitle, cardPrice, cardImage, id }) => (
      <div className="col">
        <ProductCard
          incrementCard={props.incrementCard}
          id={id}
          cardTitle={cardTitle}
          cardPrice={cardPrice}
          cardImage={cardImage}
        />
      </div>
    ),
  );
  return (
    <div className="container">
      <div className="row">{productList}</div>
    </div>
  );
};
