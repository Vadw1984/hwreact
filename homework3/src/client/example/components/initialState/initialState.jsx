export const initialState = {
  products: [
    {
      id: 'edfr4',
      cardTitle: 'Orange',
      cardPrice: 0.5,
      cardImage: 'http://www.azspagirls.com/files/2010/09/orange.jpg',
      modalTotalCount: 0,
      ProductTotalPrice: 0,
    },
    {
      id: 'edfr5',
      cardTitle: 'Banana',
      cardPrice: 1.22,
      cardImage:
        'http://images.all-free-download.com/images/graphicthumb/vector_illustration_of_ripe_bananas_567893.jpg',
      modalTotalCount: 0,
      ProductTotalPrice: 0,
    },
    {
      id: 'edfr6',
      cardTitle: 'Lemon',
      cardPrice: 5,
      cardImage:
        'https://3.imimg.com/data3/IC/JO/MY-9839190/organic-lemon-250x250.jpg',
      modalTotalCount: 0,
      ProductTotalPrice: 0,
    },
  ],
  isModalOpen: false,
  modalTotalPrice: 0,
};
