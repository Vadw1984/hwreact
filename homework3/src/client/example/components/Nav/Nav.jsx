import React from 'react';
import { ButtonClick } from '../../../../shared/components/ButtonClick/ButtonClick';

export const Nav = props => {
  let count = 0;
  props.products.forEach(item => {
    count = item.modalTotalCount + count;
    return count;
  });

  return (
    <nav className="navbar navbar-inverse bg-inverse fixed-top bg-faded">
      <div className="row">
        <div className="col">
          <ButtonClick
            onClick={() => props.openModal()}
            className="btn btn-primary"
            text={`Cart (${count})`}
          />
          <ButtonClick
            onClick={() => props.clearAllCard()}
            className="clear-cart btn btn-danger"
            text={`Clear Cart`}
          />
        </div>
      </div>
    </nav>
  );
};
