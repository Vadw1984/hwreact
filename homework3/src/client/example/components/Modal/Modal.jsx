import React from 'react';

export const Modal = ({
  products,
  modalTotalPrice,
  isModalOpen,
  incrementCard,
  decrementCard,
  clearCurrentCard,
  openModal,
  }) => {
   const modalCards = products.map(
    
    ({ modalTotalCount, cardTitle, cardPrice, id }) => {
      if (modalTotalCount > 0) {
        return (
          <table className="show-cart table">
            <tr>
              <td>{cardTitle}</td>
              <td>{cardPrice}</td>
              <td>
                <div class="input-group">
                  <button
                    onClick={() => decrementCard(id)}
                    class="minus-item input-group-addon btn btn-primary">
                    -
                  </button>
                  <input
                    type="number"
                    class="item-count form-control"
                    data-name='" + cartArray[i].name + "'
                    value={modalTotalCount}
                  />
                  <button
                    onClick={() => incrementCard(id)}
                    class="plus-item btn btn-primary input-group-addon">
                    +
                  </button>
                </div>
              </td>
              <td>
                <button
                  onClick={() => clearCurrentCard(id)}
                  class="delete-item btn btn-danger"
                  data-name=" + cartArray[i].name + ">
                  X
                </button>
              </td>
              <td>{modalTotalCount * cardPrice}</td>
            </tr>
          </table>
        );
      }
    },
  );

  return (
    <div
      className={isModalOpen ? 'modal fade' : ''}
      id="cart"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true">
      <div className="modal-dialog modal-lg" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Cart
            </h5>
            <button
              onClick={() => openModal()}
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            {modalCards}
            <div>
              Total price: $
              <span className="total-cart">{modalTotalPrice}</span>
            </div>
          </div>
          <div className="modal-footer">
            <button
              onClick={() => openModal()}
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal">
              Close
            </button>

            <button
              onClick={() => openModal()}
              type="button"
              className="btn btn-primary">
              Order now
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
