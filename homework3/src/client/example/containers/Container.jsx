import React, { useEffect, useReducer } from 'react';
import { initialState } from '../components/initialState/initialState';
import { Main } from '../components/Main';
import { Modal } from '../components/Modal/Modal';
import { modalReducer } from '../components/ModalReducer/ModalReducer';
import { Nav } from '../components/Nav';

const Container = () => {
  
  const getLocalStorageState = ()=>{if (JSON.parse(localStorage.getItem('productCardData')) === null) {
    return initialState;
  }
  return JSON.parse(localStorage.getItem('productCardData'))
}
  const [productCardData, dispatch] = useReducer(modalReducer, getLocalStorageState());

  localStorage.setItem("productCardData", JSON.stringify(productCardData))

  const openModal = () => dispatch({ type: 'OPEN_MODAL' });
  const incrementCard = id => dispatch({ type: 'INCREMENT_CARD', elem: id });
  const decrementCard = id => dispatch({ type: 'DECREMENT_CARD', elem: id });
  const clearAllCard = () => dispatch({ type: 'DELETE_ALL_PRODUCT' });
  const clearCurrentCard = id => dispatch({ type: 'CLEAR_CURRENT_CARD', elem: id });

   useEffect(() => {
    let allSum = 0;
    productCardData.products.map(el => {
      let productSum = el.modalTotalCount * el.cardPrice;
      allSum = productSum + allSum;
      return dispatch({ type: 'SET_ALL_SUM', elem: allSum.toFixed(2) });
    });
  }, [productCardData.products]);

   return (
    <div className="container">
      <Nav
        {...productCardData}
        openModal={openModal}
        clearAllCard={clearAllCard}
      />

      <Main incrementCard={incrementCard} />

      <Modal
        {...productCardData}
        isModalOpen={!productCardData.isModalOpen}
        openModal={openModal}
        incrementCard={incrementCard}
        decrementCard={decrementCard}
        clearCurrentCard={clearCurrentCard}
      />
    </div>
  );
};

export default Container;
