import React from 'react';

export const ProductCard = props => {
 
  const { cardTitle, cardPrice, cardImage, incrementCard, id } = props;

  return (
    <div className="card" style={{ width: '20rem' }}>
      <img className="card-img-top" src={cardImage} alt="Card image cap" />
      <div className="card-block">
        <h4 className="card-title">{cardTitle}</h4>
        <p className="card-text">Price: ${cardPrice}</p>
        <a
          onClick={() => incrementCard(id)}
          href="#"
          data-name={cardTitle}
          data-price={cardPrice}
          className="add-to-cart btn btn-primary">
          Add to cart
        </a>
      </div>
    </div>
  );
};
