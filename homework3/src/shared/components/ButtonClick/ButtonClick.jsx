import React from 'react';

export const ButtonClick = props => {
  return (
    <button
      onClick={props.onClick}
      type="button"
      className={props.className}
      //   data-toggle="modal"
      //   data-target="#cart"
    >
      {props.text}
    </button>
  );
};
