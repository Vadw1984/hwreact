import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/scss/style.scss';
    
import Container from '../../../client/example/containers/Container';

export const App = () => {
  return <Container />;
};
